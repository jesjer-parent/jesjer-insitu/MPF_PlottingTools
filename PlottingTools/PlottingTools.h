#ifndef PlottingTools_PlottingTools_h
#define PlottingTools_PlottingTools_h

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TStyle.h>
#include "TLatex.h"
#include <TLine.h>

#include "SystTool/SystTool.h"
#include "SystTool/SystContainer.h"
#include "SystTool/Utils.h"

#include "JES_ResponseFitter/JES_BalanceFitter.h"


//#include "/home/ajhorton/atlasstyle-00-03-04/AtlasUtils.h"
#include "AtlasUtils.h"
#ifndef __CINT__
#include "AtlasStyle.C"
#include "AtlasLabels.C"
#endif


using namespace std;

namespace PlottingTools {

  // ___________________________________________________________

  void PrepareTwoPadCanvas( TCanvas* canvas,
                            TString xTitle, TString yTitle1, TString yTitle2,
                            float xmin, float xmax,
                            float ymin1, float ymax1,
                            float ymin2, float ymax2,
                            int nDivisions);

  void PrepareOnePadCanvas( TCanvas* canvas,
                            TString xTitle, TString yTitle,
                            float xmin, float xmax,
                            float ymin, float ymax,
                            bool setLogX = false, bool setLogY = false );

  TGraphErrors* MakeGraphFromHisto(TH1D *histo, TString name, bool ignoreYerrors, bool ignoreXerrors, bool abs, double scale=1.);
  TH1D*         MakeHistoFromGraph(TGraphErrors *graph, TString name, bool ignoreYerrors, bool ignoreXerrors, bool abs, double scale=1.);

  void SetGraphStyle(TGraph* gr, int mstyle, float msize, int mcol, int lstyle=1, int lwid=1, int lcol=1);
  void SetGraphStyle(TGraph* gr, int markerCol=1);

  void PrintLatex( const char* text,
                   double xpos, double ypos,
                   double xmax, double xmin,
                   double ymax, double ymin,
                   bool setNDC = false,
                   float fsize = 0.04);

  void PrintLatexTString( const TString& text_str,
                   double xpos, double ypos,
                   double xmax, double xmin,
                   double ymax, double ymin,
                   bool setNDC = false,
                   float fsize = 0.04) {
     PrintLatex(text_str.Data(), xpos, ypos, xmax, xmin, ymax, ymin, setNDC, fsize);
  }

  void PrintLatexZLJetInfo(std::string channel,
                   double xpos, double ypos,
                   double xmax, double xmin,
                   double ymax, double ymin,
                   bool setNDC = false,
                   float fsize = 0.04, float line_spacing = 1.5);


  void ResponseVs( TH2D* hist2d, TCanvas* binnedRespCan, TGraphErrors* RespVs, const std::string& prefix, const std::string& variable, bool isdPhi=false,double minNeff=10, double minNeffFit=30);
  TLine *MakeTLine(double x1, double y1, double x2, double y2, int col = 2);

  TGraphErrors* GetResponse(TFile *file, TString name);

  TH1D*  RemoveFirstPoint(TH1D *h1);

  void CopyHistoStyle(TH1D *h1, TH1D *h2);
  void CopyHistoStyle(TH1D *h1, TGraph *h2);
  void CopyHistoStyle(TGraph *h1, TH1D *h2);
  TF1* PlotConstantLine( float y, float xmin, float xmax, int lineType, int lineWidth, bool drawTheLine=false );

  TF1 FitResponse( TGraphErrors* g, double xmin, double xmax,
                   bool usePWR, bool useLOG2, bool useLOG3, bool useLowETbiasTerm );
  double Wigmans( double* x, double* p );
  double Groom( double* x, double* p );

// ___________________________________________________________

  vector<double> vectorize(string bins)
  {
    istringstream stream(bins);
    string bin;
    vector<double> output;

    while (getline(stream, bin, ',')) {
        output.push_back( atof(bin.c_str()) );
    }
    return output;
  }

  vector<string> vectorizeStr(string bins)
  {
    istringstream stream(bins);
    string bin;
    vector<string> output;

    while (getline(stream, bin, ',')) {
        while (bin[0] == ' ') bin.erase(bin.begin());
        output.push_back(bin);
    }
    return output;
  }


  // ___________________________________________________________

  bool EndsWith(const std::string &str, const std::string &suffix)
  {
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
  }

  // ___________________________________________________________

  vector<double> GetBins(TH1D* hist)
  {
    vector<double> bins;
    for (int i = 0; i < hist->GetNbinsX(); ++i) {
      bins.push_back(hist->GetBinLowEdge(i));
    }
    return bins;
  }

  // ___________________________________________________________

  template<typename TH1X, typename X> 
  TH1X*  RestrictHistoRange(TH1X* h1, const X& xmin, const X& xmax) {
      vector<double> ptBins;
      for( int i=1; i<=h1->GetNbinsX()+1; ++i ) {
        X x = h1->GetBinCenter(i);
        if( x < xmin or x > xmax )
          continue;
        //add the low bin edge for the first bin to be considered
        if( ptBins.empty() )
          ptBins.push_back(h1->GetXaxis()->GetBinLowEdge(i));
        ptBins.push_back(h1->GetXaxis()->GetBinUpEdge(i));
      }

      //ceate a new histo with bins yhat fall into the restricted range
      int itmp = 1;
      TH1X* tmp = new TH1X(h1->GetName(), h1->GetTitle(), ptBins.size()-1, ptBins.data());
      for( int i=1; i<=h1->GetNbinsX()+1; ++i) {
        X x = h1->GetBinCenter(i);
        if( x < xmin or x > xmax )
          continue;
        tmp->SetBinContent(itmp, h1->GetBinContent(i)); 
        tmp->SetBinError(itmp, h1->GetBinError(i));
        itmp++;
      }
      return tmp;
  }

  template<typename TGraphX, typename X> 
  TGraphX*  RestrictGraphRange(TGraphX* g1, const X& xmin, const X& xmax) {
      std::cout<<"RestrictGraphRange"<<std::endl;
      vector<X> ptCent;
      //fill pT poit positions that fall into the requested range
      std::cout<<"RestrictGraphRange :: filling pt"<<std::endl;
      std::cout<<"g1->GetN() : "<<g1->GetN()<<std::endl;
      for( int i=0; i<=g1->GetN(); ++i ) {
        double x = g1->GetX()[i];
        std::cout<<"xVal : "<<x<<std::endl;
        if( x < xmin or x > xmax )
          continue;
        ptCent.push_back(g1->GetX()[i]);
      }

      //new graph with the restricted X range
      std::cout<<"RestrictGraphRange :: newGraph"<<std::endl;
      TGraphX* tmp = new TGraphX(ptCent.size());
      tmp->SetName(g1->GetName());
      tmp->SetTitle(g1->GetTitle());
      //fill only those points, that fall into the X range. ge reverse in bins to allow simple poping from the end or the vector
      for( int i=g1->GetN(); i>=0; i--) {
        double x = g1->GetX()[i];
        if( x < xmin or x > xmax )
          continue;
        int gXId = ptCent.size()-1;
        tmp->SetPoint( gXId, ptCent.back(), g1->GetY()[i] );
        tmp->SetPointError( gXId, g1->GetErrorX(i), g1->GetErrorY(i) );
        ptCent.pop_back();
      }

      std::cout<<"RestrictGraphRange :: returning graph"<<std::endl;
      return tmp;
  }


}

#endif


