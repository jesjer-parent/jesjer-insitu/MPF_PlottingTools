Plotting tools for V+jet insitu calibration
=========================

This is a spin-off of the original [PlottingTools on svn](http://svnweb.cern.ch/world/wsvn/atlas-ahorton/PlottingTools/trunk).
This version is developed to compile and run in ATLAS SW release 21.2+ (cmake+git).

At the moment there has been no clean-up done and all source files are kept.
Later on we will need to restructure the directory tree.

The follwoing instructions are supposed to work on lxplus out-of-the-box 
if you've used the [MPFPlotting](https://gitlab.cern.ch/mlisovyi/MPFPlotting) 
and set up the environment following the instructions given there.
The `MPFPlotting` wrapper package will also come with a `run_chain.sh`
script that illustrates how to run the full analysis chain from input ROOT
files with Bootstrap objects up to the final paper-quality plots and the inputs
for the insitu combination.
The current instructions are based on the large-R jet analysis, 
they need minor adjustments for small-R counter-partners analyses. 

Usage of the tools
------------------

All the macros that you can use are in the `util` directory. 
The general advice is **to execute them from a dedicated `run` directory**, 
as all of them dump output in the current directory.
Here the most basic ones:

1. Fits of response in each `pTref` bin and responce as a function of the `pTref`:
```bash
PlotResponse --file <yourfilename.root>
```
(uses the `hResponseProbePT` histogram be default (corresponds to MPF),
you **will want to use `hBalanceProbePT` instead**, if you are interested in DirectBalance method, 
see below how to change it).  
Outputs are stored *in the current directory*.
Run this tool on all inputs, as it adds mean responce graphs, that are later used by `doSyst`.
Input arguments are:
   * `--file`: the input file name with histograms;
   * `--histname`: the name of the histogram to be used (default is `hResponseProbePT`);
   * `--doFit`: fit the extracted pT dependence of the responce (boolean flag, default is `False`);
   * `--scaleLabel`: a string to label the jet scale on the plots (default is empty string);
   * `--outPlotFormat`: a string to define a comma-separated list of formats of the output plots (default is `pdf`).


2. Extract mapping between `pTjet` and `pTref` (the final results will be expressed as a function of `pTjet`)
```bash
Mapping --file <yourfilename.root>
```
Outputs are stored *in the current directory*.
Additionally, the script writes into the input file a mapping graph, that is used by `doSyst`.
Make sure that you run this script on the **nominal MC** file, as mapping is read out from there.  
Input arguments are:
   * `--file`: the input file name with histograms;
   * `--histname`: the name of the histogram to be used (default is `hMappingProbePT`, 
   no need to change, as it is the same histo for MPF and DB);
   * `--scaleLabel`: a string to label the jet scale on the plots (default is empty string);
   * `--outPlotFormat`: a string to define a comma-separated list of formats of the output plots (default is `pdf`).


3. Process systematics
```bash
doSyst --data <yourdatafile.root> --MC <yournominalMCfile.root> --altMC <youralternativeMCfile.root> --jetAlg AntiKt10LCTopo --calibType ZLjet --measurement bal
``` 
will compute the systematic uncertainty values and generate three root files: `syst_JES.root`, `rebinned.root` and `toys.root`,
that are used in the next plotting macro. 
`toys.root` is in fact not used anywhere by this sequence of tools.
This script will also **produce the plot comparing mean response in data and MC's**.
Here the mapping between the `pTjet` and `pTref` from step #2 is actually used; 
however, it **only fixes the bins centers**
but not the edges and you may find some issues related to bin edges out of order.
In such case you need to tune the custom left edge of the first bin in 
[here](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/MPF_PlottingTools/blob/fa421d91540c95e7b1c1a5464dcf39511af9d22f/util/doSyst.cxx#L189-194).
Outputs are stored *in the current directory*.

    | Input arguments | Description |
    |---------|-------------|
    |`--data`| input data file name with histograms|
    |`--MC`| nominal MC file with histos|
    |`--altMC`| alternative MC file with histos|
    |`--jetAlg`| jet algorithm for histogram names|
    |`--calibType`| analysis type (choose between `Zjet`, `ZLjet` and `Gjet`). This argument is used to set channel-specific configuration (e.g. different bins)|
    |`--measurement`| measurement type (choose between `mpf` and `bal`)|
    |`--output`| the name of the output file (the default is *syst_JES.root*)|
    |`--DataLabel` | a text label that will show up on the plots |
    |`--ScaleLabel`| a text label that will show up on the plots |
    |`--smoothNSigma` | nSigma for smoothing of results in the rebinned output|
    |`--outPlotFormat`| comma-separated list of formats of the output plots (default is `pdf`) |
    |`--minPT`, `--maxPT`| min and max pT of the mapped bin centers to be considered for systematics and plotting (defaults are 0 and 1e8, respectively) |
    |`--doCapMCSystLowPtLargeR`| clip the modelling systematics at low pT (all bins below a certain bin are set to that bin value). This was introduced in large-R approval process to have non-zero modelling uncertainty |
    |`--plotStatus`| comma-separated list of ATLAS-status prefixes (default is `Internal` and typical use case is `Internal,Preliminary,Paper`). The upper-case version is added to the output file name |



4. Plot systematics 
```bash
plotSyst --file syst_JES.root
```
will **plot a comparison of all the uncertainties together with the total uncertainty**.  
Input arguments are:
   * `--file`: the input file name with histograms;
   * `--outPlotSuffix`: a string suffix to be added to the output files (default is an empty string);
   * `--outPlotFormat`: a string to define a comma-separated list of formats of the output plots (default is `pdf`);
   * `--plotStatus`: a string to define a comma-separated list of ATLAS-status prefixes (default is `Internal` and typical use case is `Internal,Preliminary,Paper`). The upper-case version is added to the output file name.


There are many more macros that can be used to perform different studies but these are the ones we are using.
The tools listed above were tested to compile and to run on sample histogram test files from the gamma+jet analysis.


