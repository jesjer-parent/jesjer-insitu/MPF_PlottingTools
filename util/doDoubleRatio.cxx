#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

#include <TFile.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLegend.h>


#include "PlottingTools/PlottingTools.h"

using namespace PlottingTools;

/** Example program to get JES systematics */
int main(int argc, char *argv[])
{
  SetAtlasStyle();
  int ip=1;
  int iplast = ip;
  char _namebuffer[100];
  std::string DataFileName = "syst_data_AntiKt4LCTopo.root"; 
  std::string MCFileName   = "syst_MC_AntiKt4LCTopo.root";
  string JetAlg        = "AntiKt4LCTopo";
  string _measurement  = "mpf";
  TString _outfile     = "syst_JES.root"; 

  while (ip<argc) {
    cout << "ip is " << ip << endl;
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--data") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          DataFileName=argv[ip+1];
          ip+=2;
          cout << "should be in here " << ip << endl;
        } else {std::cout<<"\nno Data file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--MC" or string(argv[ip])=="--mc") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          MCFileName=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno MC file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--jetAlg" or string(argv[ip])=="--alg") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          JetAlg=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno jet algorithm specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--output") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outfile=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno output specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--measurement") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _measurement=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno output specified"<<std::endl; break;}
      }
    }
    if (ip == iplast) {cout << "problem with arguments " << ip << " " << iplast <<  endl; return 1;}
    iplast = ip; 
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Comparing " << DataFileName << " to " << MCFileName << endl;
  cout << "Jet Alogorithm set to: " << JetAlg << endl;
  cout << endl;
  cout << "These settings can be changed using " << endl;
  cout << "doSyst --data MyDataFileName.root --mc MyMCFileName.root --alg JetAlgorithmUsed" << endl << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;
  
  TFile* tmpFile = TFile::Open(TString(DataFileName), "read");
  TList* list = tmpFile->GetListOfKeys();
  TIter next(list); 
  TObject* object = 0; 
  vector<string> systNames;

  while ((object = next())) {
    if ( strstr((object->GetName()), "bootstrap_mpf") == NULL) continue;
    else {
      string tmpFile(object->GetName());
      string tmp2 = tmpFile.erase(0, 14);
      systNames.push_back(tmp2);
    }
  }
  
 
  TFile *output = TFile::Open(_outfile, "recreate");
    
  TFile* data = TFile::Open(TString( TString(DataFileName))); //"syst_data_" + JetAlg + ".root"));
  TFile* MC   = TFile::Open(TString( TString(MCFileName))); //"syst_MC_" + JetAlg + ".root"));

  TCanvas* cComp = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 700, 780 );
  cComp->Divide( 1, 2, 0.0, 0.01, 0 );
  cComp->cd(1);
  gPad -> SetLogx();
  cComp->cd(2);
  gPad -> SetLogx();

  double xMax = 1000;
  double xMin = 10;
  double yMax = 1.19001;
  double yMin = 0.8001;
  PrepareTwoPadCanvas( cComp, "P_{T}^{Z}", "MPF/MC", "Ratio", xMin, xMax, yMin, yMax, 0.9501, 1.04999, 505);
//  PrepareTwoPadCanvas( cComp, "P_{T}^{Z}", "MPF/MC", "Ratio", 10, 1000, 0.8001, 1.19001, 0.7001, 1.29999, 505);

  TGraphErrors* DataGraph = (TGraphErrors*) data->Get("");
  DataGraph->SetMarkerStyle(20);
  DataGraph->SetMarkerSize(1.0);
  DataGraph->SetMarkerColor(1);
  
  TGraphErrors* MCGraph   = (TGraphErrors*) MC->Get("");
  MCGraph->SetMarkerStyle(20);
  MCGraph->SetMarkerSize(1.0);
  MCGraph->SetMarkerColor(2);

  TGraphErrors* gRatio = new TGraphErrors(); 
  gRatio->SetMarkerStyle(20);
  gRatio->SetMarkerSize(1.0);
  gRatio->SetMarkerColor(1);


  for ( int n = 0; n < DataGraph->GetN(); ++n ) {
    if (DataGraph->GetY()[n] == 0 or MCGraph->GetY()[n]==0) continue;
    if (DataGraph->GetY()[n] == -999 or MCGraph->GetY()[n]==-999) continue;
    gRatio->SetPoint(n, 0.5*(DataGraph->GetX()[n] + MCGraph->GetX()[n]), DataGraph->GetY()[n] / MCGraph->GetY()[n]);
    cout << (0.5*(DataGraph->GetX()[n] + MCGraph->GetX()[n])) << " " << (DataGraph->GetY()[n] / MCGraph->GetY()[n]) << " " << DataGraph->GetY()[n] << " " << DataGraph->GetEY()[n] << endl;
    double yerr = DataGraph->GetY()[n] / MCGraph->GetY()[n] * sqrt( pow(DataGraph->GetEY()[n]/DataGraph->GetY()[n],2) + pow(MCGraph->GetEY()[n]/MCGraph->GetY()[n],2) );
    gRatio->SetPointError(n, DataGraph->GetEX()[n], yerr);
  }

  TLegend *leg = new TLegend(0.6,0.1,0.8,0.3,NULL,"brNDC");
  leg->SetBorderSize(0);

  leg->AddEntry(DataGraph, "Pythia", "lp");
  leg->AddEntry(MCGraph,   "Madgraph", "lp");
//  leg->SetHeader("Ratio");



  TF1 *LineAtCenter   = PlotConstantLine( 1.0 , xMin, xMax, 2, 1 );
  TF1 *LineAtMinus    = PlotConstantLine( 0.99, xMin, xMax, 3, 1 );
  TF1 *LineAtPlus     = PlotConstantLine( 1.01, xMin, xMax, 3, 1 );
  TF1 *LineAtTwoMinus = PlotConstantLine( 0.98, xMin, xMax, 3, 1 );
  TF1 *LineAtTwoPlus  = PlotConstantLine( 1.02, xMin, xMax, 3, 1 );

  LineAtCenter->Draw("sameL");
  LineAtMinus->Draw("sameL");
  LineAtPlus->Draw("sameL");
  LineAtTwoMinus->Draw("sameL");
  LineAtTwoPlus->Draw("sameL");

  cComp->cd(1);
  DataGraph->Draw("sameP");
  MCGraph->Draw("sameP");
  leg->Draw("same");
  TLatex *latex = new TLatex();
  latex->SetTextColor(1);
  latex->SetTextFont(42);
  latex->SetTextSize(0.04);
  ATLASLabel(0.27,0.83, "Preliminary", true);
  latex->DrawLatex( 0.01*(xMax-xMin)+xMin, 0.83*(yMax-yMin)+yMin, "#sqrt{s}=13 TeV, MPF with Z-jet");
  latex->DrawLatex( 0.01*(xMax-xMin)+xMin, 0.77*(yMax-yMin)+yMin, "anti-k_{t} R=0.4 EM+GSC, |#eta_{jet}| < 0.8");

  cComp->cd(2);
  gRatio->Draw("sameP");
  
//  gRatio->SetName("Method1");
  gRatio->SaveAs("Ratio.root");
  
   
  sprintf( _namebuffer, "DoubleRatio.pdf", _measurement.c_str());
  cComp->SaveAs(_namebuffer);
  output->cd();
  output->Close();
  return 0;
}

