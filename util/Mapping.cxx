#include "JES_ResponseFitter/JES_BalanceFitter.h"
#include "PlottingTools/PlottingTools.h"
#include <TFile.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TFrame.h>

using namespace std;
using namespace PlottingTools;

void DrawHisto(TH1F *h, TString ytit, double min, double max) {
  h->GetYaxis()->SetRangeUser(min,max); h->SetXTitle("#it{p}_{T}^{ref} [GeV]"); h->SetYTitle(ytit);
  h->SetStats(0); h->Draw();
}

int main(int argc, char **argv) {
  SetAtlasStyle();
  gROOT->SetBatch(kTRUE);
  gROOT->ForceStyle(kTRUE);

  char _namebuffer[100];

  string _histname   = "hMappingProbePT";
  string _filename   = "";
  string _scaleLabel = "";
  string _outPlotFormat    = "pdf";
  
  int ip=1;
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--histname") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _histname=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--file") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _filename=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno histogram name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--scaleLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _scaleLabel=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno scale name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--outPlotFormat") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outPlotFormat = argv[ip+1];
          ip+=2;
        } else {cout<<"\nno outPlotFormat specified"<<std::endl; break;}
      }
    }
    /////////////////////////////////////////////////
    // If no options specified assume there's only
    // one argument and that argument is the filename
    /////////////////////////////////////////////////
    else { 
      cout << "Arguments must be specified using --file or --histname" << endl;
      cout << "Since this was not done assuming the first argument given is the filename" << endl;
      cout << "Using 'hMappingProbePT' as the histname" << endl;
      _filename=argv[ip];
      ip=argc;
    }
  }
  //check that the filename was set
  if( _filename.empty() ) {
    cout<<"No filename was set! Exiting..."<<endl;
    return 1;
  }

  // ////////////////////////////////////////////////
  // How far from the arithmatic mean do we allow the
  // fir to see?
  // ////////////////////////////////////////////////
  double NsigmaForFit = 100.6;
  
  // ////////////////////////////////////////////////
  // Min number of events required to attempt a fit. 
  // Use mean for min_N_eff < n <= min_N_eff_for_fit, 
  // -999 otherwise
  // ////////////////////////////////////////////////
  double _min_N_eff = 10;
  double _min_N_eff_for_fit = 9999999.;

  // ////////////////////////////////////////////////
  // Vectors to store the fitted and arithmatic means
  // of the response distributions for each Pt bin
  // ////////////////////////////////////////////////
  vector<double> _mpfmean;
  vector<double> _mpfmeanError;
  vector<double> _mpfgausfit;
  vector<double> _mpfgausfitError;

 
  char prefix[40];
  sprintf( prefix, "Mapping");

  float xmin = 10.;
  float xmax = 2000.;
  float ymin = 10;
  float ymax = 2000;

  // /////////////////////////////////////////////////
  // Canvas which will hold the Jet pT distributions
  // for each Reference pT bin
  // /////////////////////////////////////////////////
  sprintf( _namebuffer, "%s_distributions_in_PtRef_bins", prefix);
  TCanvas* binnedJetVsRefPtCan = new TCanvas( _namebuffer, _namebuffer, 2100, 1500 );


  // The graph which will display Jet Vs Ref Pt
  TGraphErrors* JetVsRefPt = new TGraphErrors();
  sprintf( _namebuffer, "%sVsProbePT_graph", prefix );
  JetVsRefPt->SetName(_namebuffer);
  JetVsRefPt->SetTitle(_namebuffer);


  gErrorIgnoreLevel=2000; // removes Canvas print statements
  TFile *_infile = TFile::Open(_filename.c_str(), "UPDATE");

  // /////////////////////////////////////////////////
  // get bin center mapping into JetVsRefPt
  // /////////////////////////////////////////////////
  TH2D* hist2d = (TH2D*) _infile->Get(_histname.c_str());
  ResponseVs( hist2d, binnedJetVsRefPtCan, JetVsRefPt, prefix, "p_{T}^{Ref}", false, _min_N_eff, _min_N_eff_for_fit);

  //the Large-R scenario
  if( hist2d->GetXaxis()->GetBinLowEdge(1) >= 50) {
    xmin = 80.;
    xmax = 1000.;
    ymin = 80.;
    ymax = 1000.;
  }

  //save jet pT in individual ref pT bins
  size_t istart = _filename.find_last_of('/')+1;
  size_t iend = _filename.size()-5;
  string NameShort = _filename.substr(istart, iend-istart);
  for(string outFormat: PlottingTools::vectorizeStr(_outPlotFormat + ",root")) {
    sprintf( _namebuffer, "%s_%sVsPt_bins.%s", NameShort.c_str(), prefix, outFormat.c_str());
    binnedJetVsRefPtCan->SaveAs(_namebuffer);
  }

  // //////////////////////////////////////////////////
  // Canvas and graph which will display MenJet Vs GeomAveRef Pt
  // //////////////////////////////////////////////////
  sprintf( _namebuffer, "%s_vs_PtRef", prefix);
  TCanvas* JetVsRefPtCan = new TCanvas( _namebuffer, _namebuffer, 200, 10, 700, 780 );
  JetVsRefPtCan->Divide( 1, 2, 0.0, 0.01, 0 );
  JetVsRefPtCan->cd(1);
  gPad -> SetLogx();
  gPad -> SetLogy();
  JetVsRefPtCan->cd(2);
  gPad -> SetLogx();
  PrepareTwoPadCanvas( JetVsRefPtCan, "p_{T}^{Ref}", "p_{T}^{JES+GSC}", "p_{T}^{JES+GSC}/p_{T}^{Ref}", xmin, xmax, ymin, ymax, 0.1001, 1.499, 505);// 0.8001, 1.199 if ratio range doesn't fit


  JetVsRefPtCan->cd(1);
  // draw the mapped jet pT vs Ref pT
  JetVsRefPt->SetMarkerStyle(20);
  JetVsRefPt->SetMarkerSize(1.0);
  JetVsRefPt->SetMarkerColor(1);
  JetVsRefPt->SetLineWidth(1);
  JetVsRefPt->SetLineColor(1);
  JetVsRefPt->Draw("sameP0");

  // draw the y=x line to guide the eye
  TF1* f = new TF1( "f", "x", xmin, xmax );
  f->SetLineStyle(2);
  f->SetLineColor(1);
  f->SetLineWidth(1);
  f->Draw("sameL"); 

  // Just here temporarily, litterally just ripped out of Jiri's old plotting macro
  TLatex *p = new TLatex(200, 0.6, _scaleLabel.c_str());
  p->SetTextFont(42);
  p->SetTextColor(1);
  p->Draw();
  ATLASLabel(0.18, 0.87, "Internal", true);

  JetVsRefPtCan->cd(2);
  //the graph to store the ratio
  TGraphErrors* gRatio = new TGraphErrors();
  gRatio->SetMarkerStyle(20);
  gRatio->SetMarkerSize(1.0);
  gRatio->SetMarkerColor(1);
  //calculate the ratio graph points
  cout<<"Ref_pT  Jet_pT  Jet/Ref  Ref-Jet"<<std::endl;
  for ( int n = 0; n < JetVsRefPt->GetN()-1; ++n ) {
    double jetToRefRatio = JetVsRefPt->GetY()[n] / JetVsRefPt->GetX()[n];
    cout << JetVsRefPt->GetX()[n] << "    " << JetVsRefPt->GetY()[n] << "    " << jetToRefRatio << "    " << JetVsRefPt->GetX()[n] - JetVsRefPt->GetY()[n] << endl;
    gRatio->SetPoint(n, JetVsRefPt->GetX()[n] , jetToRefRatio);
    double yerr = jetToRefRatio * JetVsRefPt->GetEY()[n] / JetVsRefPt->GetY()[n];
    gRatio->SetPointError(n, JetVsRefPt->GetEX()[n], yerr); 
  }
  PlotConstantLine( 1.0 , xmin, xmax, 2, 1, true);
  PlotConstantLine( 0.90, xmin, xmax, 3, 1, true);
  PlotConstantLine( 1.10, xmin, xmax, 3, 1, true);
  gRatio->Draw("sameP");

  for(string outFormat: PlottingTools::vectorizeStr(_outPlotFormat)) { 
    sprintf( _namebuffer, "%s_%sVsPt.%s", NameShort.c_str(), prefix, outFormat.c_str());
    JetVsRefPtCan->SaveAs(_namebuffer);
  }

  //write out and overwrite the existing object to avoid confusion with multiple object instances
  JetVsRefPt		->Write(JetVsRefPt->GetName(), 		TObject::kOverwrite);
  binnedJetVsRefPtCan	->Write(binnedJetVsRefPtCan->GetName(),	TObject::kOverwrite);
  JetVsRefPtCan		->Write(JetVsRefPtCan->GetName(), 	TObject::kOverwrite);
  _infile->Close();
}
