#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <TLegend.h>
#include <algorithm> // std::transform
#include <utility> //std::pair
	

#include "PlottingTools/PlottingTools.h"

using namespace PlottingTools;
using namespace std;

std::string extractSystNameFromHistoName(const std::string& systName) {
    //decompose object name
    string tmp = systName;
    std::size_t found = tmp.find("_")+1;
    tmp = tmp.erase(0, found);
    found = tmp.find("Anti")-1;
    tmp = tmp.substr(0, found);
    found = tmp.find("__1");
    tmp = tmp.substr(0, found);
    //try both up and down string
    found = tmp.find("__down");
    tmp = tmp.substr(0, found);  
    found = tmp.find("__up");
    tmp = tmp.substr(0, found);

    return tmp;
}

// Sagitta unc name is deduced from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MuonMomentumCorrectionsSubgroup#Tools
  std::map<std::string, std::string> systNameConvertion = {
    {"EG_RESOLUTION_ALL", "#it{e E}-resolution"},
    {"EG_SCALE_ALL", "#it{e E}-scale"},
    {"JVT", "Pileup (JVT)"},
    {"MUON_ID", "#it{#mu E}-resolution ID"},
    {"MUON_MS", "#it{#mu E}-resolution MS"},
    {"MUON_SAGITTA_RESBIAS", "#it{#mu E}-scale(charge) residual"},
    {"MUON_SAGITTA_RHO", "#it{#mu E}-scale(charge)"},
    {"MUON_SCALE", "#it{#mu E}-scale"},
    {"PRW_DATASF", "Pileup (npv shift)"},
    {"Veto", "Sub-leading jet veto"},
    {"dPhi", "#Delta#it{#phi}"},
    {"Stat", "Statistical"},
    {"MC", "MC modelling"}
  };

// Line style and colour (optimised to make b&w-friendly)
// Systematics groups share line style (e.g. electron systematics has a different style wrt muon))
int ls_mu = 6;
int ls_el = 2;
int ls_pu = 10;
std::map<std::string, std::pair<int,int>> lineStyleConvertion = {
    {"EG_RESOLUTION_ALL",	std::make_pair(ls_el, kRed)},
    {"EG_SCALE_ALL",		std::make_pair(ls_el, kRed+3)},
    {"JVT",			std::make_pair(ls_pu, kGreen+1)},
    {"MUON_ID",			std::make_pair(ls_mu, kCyan-1)},
    {"MUON_MS",			std::make_pair(ls_mu, kCyan-2)},
    {"MUON_SAGITTA_RESBIAS",	std::make_pair(ls_mu, kCyan-3)},
    {"MUON_SAGITTA_RHO",	std::make_pair(ls_mu, kCyan-6)},
    {"MUON_SCALE",		std::make_pair(ls_mu, kCyan-8)},
    {"PRW_DATASF",		std::make_pair(ls_pu, kGreen+2)},
    {"Veto",			std::make_pair(1, kYellow+2)},
    {"dPhi", 			std::make_pair(1, kMagenta-4)},
    {"Stat",			std::make_pair(1, kBlue+2)},
    {"MC",			std::make_pair(1, kRed-4)}
  };


int main(int argc, char *argv[])
{
  int ip=1;
  int iplast = ip;
  string _infilename;
  bool _debug = false;
  string _binning = " ";
  bool isPhoton = true;
  bool isEM=true;
  bool isPFlow=false;
  std::string calibType = "XXX";
  std::string _outPlotFormat    = "pdf";
  std::string _outPlotSuffix    = "";
  std::string _plotStatus       = "Internal";

  SetAtlasStyle(); 
  while (ip<argc) {
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--file") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _infilename=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno file name specified"<<std::endl; break;}
      } else if (string(argv[ip])=="--debug") {
        _debug = true;
        ip+=1;
      }
      else if (string(argv[ip])=="--outPlotFormat") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outPlotFormat = argv[ip+1];
          ip+=2;
        } else {cout<<"\nno outPlotFormat specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--outPlotSuffix") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outPlotSuffix  = argv[ip+1];
          ip+=2;
        } else {cout<<"\nno outPlotSuffix specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--plotStatus") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _plotStatus = argv[ip+1];
          ip+=2;
        } else {cout<<"\nno plotStatus specified"<<std::endl; break;}
      }
    }
    if (ip == iplast) {cout << "problem with arguments(most likely an unknown argument was passed to the tool): " << argv[ip] <<  endl; return 1;}
    iplast = ip;
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Plotting systematic uncertainties found in " << _infilename << endl << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;

  map<string, TGraphErrors*> Systs;
  vector<double>ptBins;


 

  int colour_def = kSpring-9;
  int nbinsx=0;


  TFile* _infile = TFile::Open(_infilename.c_str(), "read");
  TList* list = _infile->GetListOfKeys();
  TIter next(list);
  while (TObject* object =  next()) {
    string oname(object->GetName());
    TH1D* hist = (TH1D*) _infile->Get(oname.c_str());
    if (_debug)
      cout<<"oname = "<<oname<<endl;

    //get bin boundaries and calibType from the first accessed object
    if (nbinsx == 0){
      nbinsx = hist->GetNbinsX()+1;
      if (_debug)
        std::cout << "pT bin edges [GeV]: " << std::endl;
      for (int i = 0; i < nbinsx; ++i) {
        ptBins.push_back(hist->GetXaxis()->GetBinLowEdge(i+1));
        if (_debug)
          cout << ptBins.back() << endl;
      }
      //read calibration type from the first part of the histo name
      calibType = oname.substr(0, oname.find("_"));
      //read varioius calib type bools
      if (oname.find("Z")!=std::string::npos)
        isPhoton=false;
      if (oname.find("LC")!=std::string::npos)
        isEM=false;
      if (oname.find("PFlow")!=std::string::npos){
        isEM=false;
        isPFlow=true;
      }
    }

    if (isPhoton and (oname.find("MUONS")!=std::string::npos)) 
      continue;
    if ((oname.find("Nominal")!=std::string::npos) or 
        (oname.find("NoSyst") !=std::string::npos) or 
        (oname.find("nominal")!=std::string::npos)) 
      continue;

    string systName = extractSystNameFromHistoName(oname);

    TGraphErrors *Error = MakeGraphFromHisto(hist, systName, true, true, true, 1.);
    if(Systs.find(systName) == Systs.end()){
      int lType, colour;
      if (lineStyleConvertion.find(systName) != lineStyleConvertion.end()) {
          lType  = lineStyleConvertion[systName].first;
          colour = lineStyleConvertion[systName].second;
      } else {
          // unknown systematics will all get the same style and incremental gray colours
          lType = 9;
          colour = colour_def;
          colour_def++;
      }
      SetGraphStyle(Error, 0, 0., colour, lType, 3, colour); 
 
      // For gamma+jet since we don't have Sherpa below 35 GeV I've been told to just use the same uncertainty we measure in the next bin up (35-45 Gev)
      if (isPhoton && systName=="MC") 
        Error->SetPoint(0, Error->GetX()[0], Error->GetY()[1]);
      //fill in the map
      Systs[systName] = Error;
      cout << systName << endl;
    } else {
      for (size_t ibin = 0; ibin < Error->GetN(); ++ibin) {
        double mean = (Error->GetX()[ibin] + Systs[systName]->GetX()[ibin]) / 2;
        Systs[systName]->SetPoint(ibin, mean, Systs[systName]->GetY()[ibin]);
      }
    }
  }



  TCanvas *c1 = new TCanvas("syst", "Syst", 1600, 1200);
  double xMin = 15;
  double xMax = 1600;
  double yMax = 0.10; //10, 6;
  if(calibType == "ZLjet" or calibType == "ZLJet") {
    xMin = 140;
    xMax = 750;
    yMax = 0.05;
  }
  PrepareOnePadCanvas( c1, "Large-R jet #it{p}_{T} [GeV]", "Fractional JES uncertainty",xMin, xMax, 0, yMax, true, false);

  double total[nbinsx+2];
  for (int i = 0; i < nbinsx; i++) 
    total[i] = 0;

  for (auto& Syst: Systs) { 
    int N = Syst.second->GetN();
    for (int ibin = 0; ibin <= N; ++ibin) {
      if (Syst.second->GetY()[ibin] > 1e-6){
        total[ibin] += Syst.second->GetY()[ibin] * Syst.second->GetY()[ibin];
      }
    }
  }

  TH1D *h_total = new TH1D("h_total", "h_total",nbinsx-1, ptBins.data());
  h_total->SetLineWidth(2);
  h_total->SetFillColor(kBlue-10);
  h_total->SetLineColor(1);
  h_total->SetDirectory(0);

  TLegend *leg = new TLegend(0.18,0.45,0.94,0.65,NULL,"brNDC");
  leg->SetBorderSize(0);
  leg->SetFillStyle(0);
  leg->AddEntry(h_total, "Stat. #oplus Syst.", "f");

  for (int i = 0; i <= nbinsx; i++) {
    h_total->SetBinContent(i+1,sqrt(total[i]));
  }
  h_total->Draw("C same"); 
 
 for (auto& Syst: Systs){
   //add legend entry with a cute systematics name
   string systName = Syst.first;
   string systNameLegend = systName;
   if (systNameConvertion.find(systName) != systNameConvertion.end())
     systNameLegend = systNameConvertion[systName];
   leg->AddEntry(Syst.second, systNameLegend.c_str(), "lp");
   Syst.second->Draw("C*"); //PC

   if (_debug) {
     cout<<Syst.first<<endl;
     Syst.second->Print("all");
   }
  }
  leg->SetTextFont(42);
  leg->SetNColumns(3);
  leg->Draw("same");

  gPad->RedrawAxis();

  double xfrac = 0.19; //0.01
  string mes;

  if (calibType.find("ZL") != std::string::npos) {
    // dedicated printing function for Z + large-R jet to unify format across plots
    PrintLatexZLJetInfo("", xfrac, 0.80, -1,-1,-1,-1, true, 0.05, 1.2);
  } else {
    PrintLatexTString("#sqrt{s}=13 TeV, 36.2 pb^{-1}", xfrac, 0.80, -1,-1,-1,-1, true, 0.04);

    if (isPhoton)
      mes = "MPF with #gamma-jet";
    else
      mes = "MPF with Z-jet";
    PrintLatexTString(mes.c_str(), xfrac, 0.73, -1,-1,-1,-1, true, 0.04);

    if (isEM)
      mes = "anti-k_{t} R=0.4, EM+GSC, |#eta_{jet}| < 0.8";
    else
      mes = "anti-k_{t} R=0.4, LC+GSC, |#eta_{jet}| < 0.8";
    PrintLatexTString(mes.c_str(), xfrac, 0.67, -1,-1,-1,-1, true, 0.04);
  }

  //save plots with different ATLAS status labels
  for (string ps: PlottingTools::vectorizeStr(_plotStatus)) {
      TCanvas* c1_copy = (TCanvas*) c1->Clone("c1_copy");
      c1_copy->cd(1);
      string atlasLabel = ps.find("Paper") != std::string::npos ? "" : ps;
      std::transform(ps.begin(), ps.end(), ps.begin(), ::toupper);
      cout<<ps << " -> " << atlasLabel <<endl;
      ATLASLabel(xfrac, 0.87, strdup(atlasLabel.c_str()), false);
      //save plots in different formats
      for (string outFormat: PlottingTools::vectorizeStr(_outPlotFormat)) {
          c1_copy->SaveAs(("Systs"+_outPlotSuffix+"_"+ps+"."+outFormat).c_str());
      }
      delete c1_copy;
  }
  //file with histos
  h_total->SaveAs(("TotalSyst"+_outPlotSuffix+".root").c_str());
  
}

