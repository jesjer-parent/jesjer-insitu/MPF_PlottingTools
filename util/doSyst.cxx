#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <memory>
#include <algorithm> // std::transform

#include <TFile.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLegend.h>


#include "PlottingTools/PlottingTools.h"

#define CERR  std::cout<<__LINE__<<"  --  "<<__LINE__<<std::endl;

using namespace PlottingTools;
using namespace std;

//The function to set syst histo name and title
void setSystNameAndTitle(TH1* h1, const std::string& calibType, const std::string& systN, const std::string& JetAlg);

/** Example program to get JES systematics */
int main(int argc, char *argv[])
{


  SetAtlasStyle();
  int ip=1;
  int lastround=0;
  char _namebuffer[100];
  std::string DataFileName = "syst_data_AntiKt4LCTopo.root"; 
  std::string MCFileName   = "syst_MC_AntiKt4LCTopo.root";
  string altMCFileName     = "";
  string JetAlg            = "AntiKt4LCTopo";
  string calibType         = "Zjet";
  string _measurement      = "mpf";
  string _DataLabel        = "";
  string _ScaleLabel       = "";
  TString _outfile         = "syst_JES.root"; 
  int _smoothNSigma        = 2;
  string _outPlotFormat    = "pdf";
  //max/min bins to be considered
  double _minPT            = 0.;
  double _maxPT            = 1e8;
  // a custom cap of MC syst value at low pT, as requested at the approval
  bool _doCapMCSystLowPtLargeR = false;
  string _plotStatus       = "Internal";

  while (ip<argc) {
    if (ip==lastround){cout << "check arguments " << lastround << endl; break;}
    lastround = ip;
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--data") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          DataFileName=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno Data file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--MC" or string(argv[ip])=="--mc") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          MCFileName=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno MC file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--altMC" or string(argv[ip])=="--AltMC") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          altMCFileName=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno alternative MC file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--jetAlg" or string(argv[ip])=="--alg") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          JetAlg=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno jet algorithm specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--calibType") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          calibType=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno calibration type specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--output") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outfile=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno output specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--measurement") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _measurement=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno measurement specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--DataLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _DataLabel=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno DataLabel specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--ScaleLabel") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _ScaleLabel=argv[ip+1];
          ip+=2;
        } else {cout<<"\nno ScaleLabel specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--smoothNSigma") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _smoothNSigma = atoi(argv[ip+1]);
          ip+=2;
        } else {cout<<"\nno smoothNSigma specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--outPlotFormat") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outPlotFormat = argv[ip+1];
          ip+=2;
        } else {cout<<"\nno outPlotFormat specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--minPT") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _minPT = atof(argv[ip+1]);
          ip+=2;
        } else {cout<<"\nno minPT specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--maxPT") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _maxPT = atof(argv[ip+1]);
          ip+=2;
        } else {cout<<"\nno maxPT specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--doCapMCSystLowPtLargeR") {
        _doCapMCSystLowPtLargeR=true;
        ip+=1;
      }
      else if (string(argv[ip])=="--plotStatus") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _plotStatus = argv[ip+1];
          ip+=2;
        } else {cout<<"\nno plotStatus specified"<<std::endl; break;}
      }
      else {
        cout<<"Unknown command-line option: " << argv[ip] << ". Do not know what to do with it. Exiting..."  << endl;
        return 1;
      }
    } else {
      cout<<"No input arguments?"<<endl;
    }
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Comparing " << DataFileName << " to " << MCFileName << endl;
  cout << "Jet Alogorithm set to: " << JetAlg << endl;
  cout << endl;
  cout << "These settings can be changed using " << endl;
  cout << "doSyst --data MyDataFileName.root --mc MyMCFileName.root --alg JetAlgorithmUsed" << endl << endl;
  cout << "minPT/maxPT for distributions: " << _minPT << " / " << _maxPT << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;
 
 //WARNING switched to MC pT mapping due to higher stats, butt will need to be checked for meaningfulness!
  TFile* tmpFile = TFile::Open(TString(MCFileName), "read");
  TList* list = tmpFile->GetListOfKeys();
  TIter next(list); 
  TObject* object = nullptr; 
  vector<string> systNames;

  //get the list of systematics
  while ((object = next())) {
    string oname = object->GetName();
    if (oname.find("bootstrap_"+_measurement) == string::npos) 
      continue;
    else if (oname.find("MET") != string::npos) 
      continue;
    else {
      cout << "systematics from data file " << oname.substr(14) << endl;
      systNames.push_back(oname.substr(14));
    }
  }
  
  //get pT binning
  vector<double> ptBins;
  string hname = "";
  if (_measurement == "mpf")
    hname = "Response";
  else if (_measurement == "bal") 
    hname = "Balance";
  TH2D* hist2d = (TH2D*) tmpFile->Get( ("h" + hname + "ProbePT").c_str() );
  for(int i=1; i<=hist2d->GetNbinsX()+1; ++i) 
    ptBins.push_back(hist2d->GetXaxis()->GetBinLowEdge(i));

  vector<double> mappedBins;
  if(calibType == "ZJet" or calibType == "Zjet")
    mappedBins.push_back(17.);
  else if (calibType == "ZLJet" or calibType == "ZLjet")
    mappedBins.push_back(110.);
  else
    mappedBins.push_back(28.);

  //fill the mapped pT-bin edges
  Int_t nBins=0;
  TGraphErrors* mapping = (TGraphErrors*)tmpFile->Get("MappingVsProbePT_graph");
  for ( int n = 0; n < mapping->GetN()-1; ++n ) {
    if (fabs(mapping->GetY()[n])<0.001){
      std::cout<<"MapBinning : Continuing"<<std::endl;
      continue;
    }
    if((calibType == "ZJet" or calibType == "Zjet") and n==0){
      std::cout<<"MapBinning : ZJet/Zjet"<<std::endl;
      mappedBins.push_back(24.073);  // The reco Pt Vs. ref Pt curve is too flat at low Pt to not do this. 
    }
    else{
      std::cout<<"MapBinning : NextBin"<<std::endl;
      mappedBins.push_back(2*mapping->GetY()[n]-mappedBins[n]); 
    }
    // the bin-edge-setting scheme above is intended to generate bins with bin centres at the <pTjet> values (as read from mapping->GetY())
    // but is relies on the choice of the lowest bin edge before the loop, as it fully defines whole bin-edge array given bin centres
    // bin edges do not matter in the follwoing steps, but plots will look awekward
    nBins+=1;
  }
  
  //////////////////////
  // Sam/Daniel "hack" : called "SDHack" throughout
  // use the pTref bins instead of the mapped bins because the mapped bins are dependent on the statistics
  // now the mapping is done in a later stage
  //////////////////////
  mappedBins.clear();
  for (double x: ptBins){
    std::cout<<"Transferring ptRefBin --> mappedBins : "<<x<<std::endl;
    mappedBins.push_back(x);
  }  
  

  //print out the mapped bin edges
  cout<<"pTjet bin edges (before min/max restriction) :  ";
  for (double x: mappedBins)
    cout<< x << ' '; 
  cout << endl;

  //fill a vector of bin centers that fall within min/max range
  vector<double> originalBinCenters_minmax;
  vector<double> mappedBinCenters_minmax;
  vector<double> mappedBinWidth_minmax;
  for( unsigned int i = 0; i < mappedBins.size()-1; i++ ) {
    double x = (mappedBins[i+1] + mappedBins[i]) / 2.;
    if( x > _minPT && x < _maxPT) {
      //originalBinCenters_minmax.push_back(mapping->GetX()[i]); SDHack
      originalBinCenters_minmax.push_back(x);
      mappedBinCenters_minmax.push_back(x);
      mappedBinWidth_minmax.push_back((mappedBins[i+1] - mappedBins[i]) / 2.);
    }
  }
  //print out the mapped bin edges
  cout<<"pTjet bin centers (after min/max restriction) :  ";
  for (double x: mappedBinCenters_minmax)
    cout<< x << ' '; 
  cout << endl;
  //print out the mapped bin edges
  cout<<"Original pTjet bin centers (after min/max restriction) :  ";
  for (double x: originalBinCenters_minmax)
    cout<< x << ' '; 
  cout << endl;


  //Xmin and Xmax values for the rebinning range
  double rebinXmin = mappedBinCenters_minmax.front();
  double rebinXmax = mappedBinCenters_minmax.back();

  //clean-up the input file connection
  tmpFile->Close();
  delete tmpFile; 

  //--------------------------------------------------------------------------------
  // Done with the preparation
  // Main processing starts here
  //--------------------------------------------------------------------------------

  TFile *output_original = TFile::Open( _outfile, "recreate");
  TFile *output_rebinned = TFile::Open( "rebinned.root", "recreate");
  TFile *output_toys     = TFile::Open( "toys.root", "recreate");
    
  std::cout<<"Making SystContainer : Data"<<std::endl;
  std::shared_ptr<SystContainer> systData = std::make_shared<SystContainer>(systNames, ptBins, 100);
  systData->readFromFile( DataFileName, "_"+_measurement); //"syst_data_" + JetAlg + ".root", _measurement );
      
  std::cout<<"Making SystContainer : MC"<<std::endl;
  std::shared_ptr<SystContainer> systMC   = std::make_shared<SystContainer>(systNames, ptBins, 100);
  systMC->readFromFile( MCFileName, "_"+_measurement); //"syst_MC_" + JetAlg + ".root", _measurement);
  
  std::cout<<"Making SystTool"<<std::endl;
  std::unique_ptr<SystTool> sTool = std::make_unique<SystTool>(systNames, ptBins, false);
  sTool->setNominalName("NoSyst");
  sTool->setSystData(systData.get());
  sTool->setSystMC(systMC.get());
  TH1D* Stat = 0; 
  for (string systN: systNames) { //loop over systematics
    if (systN.find("nominal") != std::string::npos) 
      continue;
    if (systN.find("NoSyst") != std::string::npos) 
      continue;
    if (systN.find("MET") != std::string::npos) 
      continue;  
    //debug printout of the systematics name
    cout << systN << endl;

    //process Bootstrap systematics toys
    sTool->runToysJES(systN);

    //the original histo with the systematics size
    //relative systemtics is stored
    //systematics effect on the nominal BS "replica" +- RMS over replicas
    TH1D* h_syst = sTool->getSystHist(JetAlg + "_" + systN);
    //the histo to store systematics size, having adjusted X axis
    //move over into the output file that will store the systematics histos 
    output_original->cd();
    TH1D* h_syst_mapped_tmp = new TH1D("", "",nBins, mappedBins.data());
    //set the histo contents
    for (int i = 1; i <= nBins; ++i) {
      h_syst_mapped_tmp->SetBinContent(i, h_syst->GetBinContent(i));
      h_syst_mapped_tmp->SetBinError  (i, h_syst->GetBinError(i));
    }
    //restrict pT range to the (min,max) value
    TH1D* h_syst_mapped = PlottingTools::RestrictHistoRange<TH1D, double>(h_syst_mapped_tmp, _minPT, _maxPT);
    //TH1D* h_syst_mapped = h_syst_mapped_tmp;
    //modify name and title
    setSystNameAndTitle(h_syst_mapped, calibType, systN, JetAlg);
    //store systematics WITHOUT rebinning
    h_syst_mapped->Write();

    //move over into the output file that will store the systematics histos BUT AFTER rebinning
    output_rebinned->cd();
    TH1D* tmp = SystUtils::rebinUntilSignificant( (*h_syst_mapped), rebinXmin, rebinXmax, _smoothNSigma);
    //modify name and title
    setSystNameAndTitle(tmp, calibType, systN, JetAlg);
    //store systematics WITH rebinning
    tmp->Write();

    //Statistical unc. Contents do not matter, as they will be reset
    if (systN == systNames.front()) 
      Stat = tmp;

    output_toys->cd();
    //get data and MC central syst histo
    TH1D *h_systMC   = sTool->getSystHistMC(JetAlg + "_" + systN);
    TH1D *h_systData = sTool->getSystHistData(JetAlg + "_" + systN);
    h_systMC->Write();
    h_systData->Write();


    ///////////////////////////////////////////////
    // Writing all of the toy distributions used to 
    // obtain the statistical uncertainty in a 
    // seperate output file
    ///////////////////////////////////////////////
    output_toys->cd();
    vector<TH1D*>* toys 	= sTool->getToysHist(JetAlg + "_" + systN);
    vector<TH1D*>* toysMC 	= sTool->getToysHistMC(JetAlg + "_" + systN);
    vector<TH1D*>* toysData 	= sTool->getToysHistData(JetAlg + "_" + systN);
    for (unsigned int i = 0; i < toys->size(); ++i) {
      toys->at(i)->Write();
      toysMC->at(i)->Write();
      toysData->at(i)->Write();
    }
  }
  
  std::cout<<"Done with systematics loop"<<std::endl;
  
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Done with simple systematics
  // Evaluate systematics due to physics modelling
  //////////////////////////////////////////////////////////////////////////////////////////////
  std::cout<<"Getting : TFile* data       : "<<DataFileName.c_str()<<std::endl;
  std::cout<<"Getting : TFile* MC         : "<<MCFileName.c_str()<<std::endl;
  std::cout<<"Getting : TFile* AltMCFile  : "<<altMCFileName.c_str()<<std::endl;
  
  std::cout<<"UsingHist : "<<(hname + "VsProbePT_graph").c_str()<<std::endl;
  std::cout<<"Lower     : "<<originalBinCenters_minmax.front()*0.99<<std::endl;
  std::cout<<"Upper     : "<<originalBinCenters_minmax.back()*1.01<<std::endl;

  TFile* data       = TFile::Open(DataFileName.c_str()); //"syst_data_" + JetAlg + ".root"));
  TFile* MC         = TFile::Open(MCFileName.c_str()); //"syst_MC_" + JetAlg + ".root"));
  TFile* AltMCFile  = TFile::Open(altMCFileName.c_str());
CERR
  TGraphErrors* DataGraph = PlottingTools::RestrictGraphRange<TGraphErrors, double>(
                                           (TGraphErrors*) data->Get((hname + "VsProbePT_graph").c_str()), 
                                           originalBinCenters_minmax.front()*0.99, originalBinCenters_minmax.back()*1.01);
  SetGraphStyle(DataGraph, 1);
CERR
  TGraphErrors* MCGraph   = PlottingTools::RestrictGraphRange<TGraphErrors, double>(
                                           (TGraphErrors*) MC->Get((hname + "VsProbePT_graph").c_str()), 
                                           originalBinCenters_minmax.front()*0.99, originalBinCenters_minmax.back()*1.01);
  SetGraphStyle(MCGraph, 2);
CERR
  TGraphErrors* gRatio = new TGraphErrors(); 
  SetGraphStyle(gRatio, 2);
CERR
  Stat->Reset();
  setSystNameAndTitle(Stat, calibType, "Stat", JetAlg);
CERR
  TH1D* PurityMeas = (TH1D*)data->Get("PurityHist");
  TH1D* Purity = (TH1D*)Stat->Clone();
  setSystNameAndTitle(Purity, calibType, "Purity", JetAlg);
CERR
  TH1D* nominal = (TH1D*)Stat->Clone();
  setSystNameAndTitle(nominal, calibType, "Nominal", JetAlg);
CERR
  TGraphErrors* AltMCGraph   = PlottingTools::RestrictGraphRange<TGraphErrors, double>(
                                           (TGraphErrors*) AltMCFile->Get((hname + "VsProbePT_graph").c_str()), 
                                           originalBinCenters_minmax.front()*0.99, originalBinCenters_minmax.back()*1.01);
  SetGraphStyle(AltMCGraph, 4);
CERR
  std::cout<<"Done with style"<<std::endl;
CERR
  TGraphErrors* MCRatio = new TGraphErrors();
  SetGraphStyle(MCRatio, 4);
CERR
  TH1D* AltMC = (TH1D*)Stat->Clone();
  setSystNameAndTitle(AltMC, calibType, "MC", JetAlg);
  TString channel="#gamma";
  TString nomMC="Pythia";
  if (calibType == "ZJet" or calibType == "Zjet" or calibType == "ZLJet" or calibType == "ZLjet") {
    channel="Z"; 
    nomMC="Powheg + Pythia8";
  }
  for ( int n = 0; n < TMath::Min(MCGraph->GetN(),DataGraph->GetN()); ++n ) {
    //if the point appears to be outside of the mapping- drop it
    if (n >= nBins) {
      MCGraph->RemovePoint(n);
      DataGraph->RemovePoint(n);
      AltMCGraph->RemovePoint(n);
      continue;
    }

    bool notValidData = (DataGraph->GetY()[n] == 0 or DataGraph->GetY()[n] == -999);
    bool notValidMC   = (MCGraph->GetY()[n]==0     or MCGraph->GetY()[n]==-999);
    if (notValidData or notValidMC) 
      continue;
    
    double xVal = mappedBinCenters_minmax[n];
    double xErr = mappedBinWidth_minmax[n];
    //switch to MC/Data as discussed and agreed in the Large-R insitu paper 2018
    double yVal = MCGraph->GetY()[n] / DataGraph->GetY()[n];
    double yErr = yVal * sqrt( pow(DataGraph->GetEY()[n]/DataGraph->GetY()[n],2) + pow(MCGraph->GetEY()[n]/MCGraph->GetY()[n],2) );

    gRatio->SetPoint(n, xVal, yVal);
    gRatio->SetPointError(n, xErr, yErr);
    Stat->SetBinContent(n+1, yErr);
    nominal->SetBinContent(n+1, yVal);

    if (channel=="#gamma") 
      Purity->SetBinContent(n+1, PurityMeas->GetBinContent(n+1)); 

    if (AltMCGraph->GetY()[n] == 0 or AltMCGraph->GetY()[n] == -999)
      continue;

    //switch to MC/Data as discussed and agreed in the Large-R insitu paper 2018
    double yValAlt = AltMCGraph->GetY()[n] / DataGraph->GetY()[n];
    double yErrAlt = yValAlt * sqrt( pow(DataGraph->GetEY()[n]/DataGraph->GetY()[n],2) + pow(AltMCGraph->GetEY()[n]/AltMCGraph->GetY()[n],2));
    MCRatio->SetPoint(n, xVal, yValAlt);
    MCRatio->SetPointError(n, xErr, yErrAlt);

    //MC model syst is still a ratio of MC1/MC2
    double ySystAlt = AltMCGraph->GetY()[n] / MCGraph->GetY()[n];
    double ySystErrAlt = ySystAlt * sqrt( pow(AltMCGraph->GetEY()[n]/AltMCGraph->GetY()[n],2) + pow(MCGraph->GetEY()[n]/MCGraph->GetY()[n],2) );
    AltMC->SetBinContent(n+1, 1-ySystAlt);
    AltMC->SetBinError(n+1, ySystErrAlt);
   
    //the graph will be used for balance distribution plotting
    AltMCGraph->SetPoint     (n, xVal, AltMCGraph->GetY()[n]); 
    AltMCGraph->SetPointError(n, xErr, AltMCGraph->GetEY()[n]);
    MCGraph->SetPoint     (n, xVal, MCGraph->GetY()[n]);
    MCGraph->SetPointError(n, xErr, MCGraph->GetEY()[n]);
    DataGraph->SetPoint     (n, xVal, DataGraph->GetY()[n]);
    DataGraph->SetPointError(n, xErr, DataGraph->GetEY()[n]);
  }
  // do the ugly custom MC uncertainty at low pT
  if (_doCapMCSystLowPtLargeR) {
    // Bin #4 is a custom choice requested in the 2015+2016 paper approval. 
    // Will have to be adjusted in the future analyses, if needed
    int binToFreeze = 4;
    for (int i=1; i<binToFreeze; i++) {
      AltMC->SetBinContent(i, AltMC->GetBinContent(binToFreeze));
      AltMC->SetBinError  (i, AltMC->GetBinError(binToFreeze));
    }
  }

  double xMax = 1600;
  double xMin = 10;
  double yMax = 1.19001;
  double yMin = 0.35001;

  if (calibType == "ZLJet" or calibType == "ZLjet"){
    xMax = 750.;
    xMin = 140;
    yMax = 1.19;
    yMin = 0.8001;
  }

  TString yTitle, yRatioTitle;
  TString xTitle = "Jet #it{p}_{T} [GeV]";
  if (_measurement == "mpf") {
    yTitle = "R_{MPF}";
  } else if (_measurement == "bal" && JetAlg=="AntiKt10LCTopo") {
    yTitle = "#it{Z}+jet direct balance, #it{R}_{DB}";
    xTitle = "Large-R jet #it{p}_{T} [GeV]";
  } else {
    yTitle = "#it{R}_{DB}";
  }
  yRatioTitle = "MC / Data";

  TCanvas* cComp = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 800, 800 );
  cComp->Divide( 1, 2, 0.0, 0.01, 0 );
  cComp->cd(1);
  gPad -> SetLogx();
  cComp->cd(2);
  gPad -> SetLogx();
  PrepareTwoPadCanvas( cComp, xTitle, yTitle, yRatioTitle, xMin, xMax, yMin, yMax, 0.9001, 1.099, 505);
  cComp->cd(1);
//  Lowest bin in for sherpa in gamma+jet doesn't make sense, don't show it
//  AltMCGraph->RemovePoint(0);
  AltMCGraph->Draw("sameP");
  DataGraph->Draw("sameP");
  MCGraph->Draw("sameP");

  //TLegend *leg = new TLegend(0.6,0.65,0.9,0.87,NULL,"brNDC");
  TLegend *leg = new TLegend(0.18,0.05,0.9,0.15,NULL,"brNDC");
  leg->SetBorderSize(0);
  leg->SetNColumns(3);
  leg->SetFillStyle(0);
  leg->SetTextFont(42);
  leg->AddEntry(DataGraph, "Data", "lp");
  leg->AddEntry(MCGraph,   nomMC, "lp");
  leg->AddEntry(AltMCGraph,   "Sherpa", "lp");
  leg->Draw("same");

  TString scale = "LC";
  if (JetAlg.find("EMTopo") != std::string::npos) 
    scale = "EM";
  if (JetAlg.find("EMPFlow") != std::string::npos) 
    scale = "PFlow";

  TString meas = "DB";
  if (_measurement == "mpf") {
    meas = "MPF";
  } 
  TString Zdecay = "";
  if (DataFileName.find("_Zee_") != std::string::npos)
    Zdecay = " (Z#rightarrowee)";
  else if (DataFileName.find("_Zmm_") != std::string::npos)
    Zdecay = " (Z#rightarrow#mu#mu)";
  else if (DataFileName.find("_Zll_") != std::string::npos)
    Zdecay = "";


  //ATLASLabel(0.20,0.87, "Internal", false, 1, 0.07);
  if (JetAlg=="AntiKt10LCTopo") {
    PrintLatexZLJetInfo(Zdecay.Data(), 0.20, 0.78, -1,-1,-1,-1, true, 0.05);
  } else {
    PrintLatexTString("#sqrt{s}=13 TeV, 36.2 pb^{-1}", 0.015, 0.85, xMax, xMin, yMax, yMin, false, 0.05);
    PrintLatexTString(meas + " with " + channel + Zdecay + " + Jet", 0.015, 0.77, xMax, xMin, yMax, yMin, false, 0.05);
    if (JetAlg=="AntiKt4EMPFlow") 
      PrintLatexTString("anti-k_{t} R=0.4, "+scale+", |#eta_{jet}| < 0.8", 0.015, 0.70, xMax, xMin, yMax, yMin, false, 0.05);
    else 
      PrintLatexTString("anti-k_{t} R=0.4, "+scale+"+GSC, |#eta_{jet}| < 0.8", 0.015, 0.70, xMax, xMin, yMax, yMin, false, 0.05);
  }
  PrintLatex(_DataLabel.c_str(), 0.30, 0.65, xMax, xMin, yMax, yMin, false, 0.05);
  PrintLatex(_ScaleLabel.c_str(), 0.30, 0.60, xMax, xMin, yMax, yMin, false, 0.05);

  cComp->cd(2);
  //fill the ratio pad
  PlotConstantLine( 1.0 , xMin, xMax, 2, 1, true );
  PlotConstantLine( 0.95, xMin, xMax, 3, 1, true );
  PlotConstantLine( 1.05, xMin, xMax, 3, 1, true );
  gRatio->Draw("sameP");
  MCRatio->Draw("sameP");

  //store outputs
  gRatio->SetName("Ratio");
  gRatio->SaveAs("CentralValue.root");

  //save plots with different ATLAS status labels
  for (string ps: PlottingTools::vectorizeStr(_plotStatus)) {
      TCanvas* cComp_copy = (TCanvas*) cComp->Clone("cComp_copy");
      cComp_copy->cd(1);
      string atlasLabel = ps.find("Paper") != std::string::npos ? "" : ps;
      std::transform(ps.begin(), ps.end(), ps.begin(), ::toupper);
      cout<<ps << " -> " << atlasLabel <<endl;
      ATLASLabel(0.20,0.87, strdup(atlasLabel.c_str()), false, 1, 0.07);
      //save plots in different formats
      for (string outFormat: PlottingTools::vectorizeStr(_outPlotFormat)) {
          sprintf( _namebuffer,
                  "%s_Ratio_%s.%s",
                  _measurement.c_str(),
                  ps.c_str(),
                  outFormat.c_str());
          cComp_copy->SaveAs(_namebuffer);
      }
      delete cComp_copy;
  }

  output_original->cd();
  Stat->Write();
  AltMC->Write();
  nominal->Write();
  if (channel=="#gamma") 
    Purity->Write();

  output_rebinned->cd();
  Stat->Write();
  TH1D* tmp = SystUtils::rebinUntilSignificant( (*AltMC), rebinXmin, rebinXmax, _smoothNSigma);
  tmp->Write();
  nominal->Write();
  if (channel=="#gamma") 
    Purity->Write();

  output_original->Close();
  output_rebinned->Close();
  return 0;
}


void setSystNameAndTitle (TH1* h1, const std::string& calibType, const std::string& systN, const std::string& JetAlg) {
    if (systN == "J2__1down") {
      h1->SetName (TString(calibType + "_" + "Veto__1down_" + JetAlg)); 
      h1->SetTitle(TString(calibType + "_" + "Veto__1down_" + JetAlg));
    }
    else if (systN == "J2__1up") {
      h1->SetName (TString(calibType + "_" + "Veto__1up_" + JetAlg)); 
      h1->SetTitle(TString(calibType + "_" + "Veto__1up_" + JetAlg));
    }
    else { 
      h1->SetName (TString(calibType + "_" + systN + "_" + JetAlg));
      h1->SetTitle(TString(calibType + "_" + systN + "_" + JetAlg));
    }
}

