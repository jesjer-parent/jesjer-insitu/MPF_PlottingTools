#include "JES_ResponseFitter/JES_BalanceFitter.h"
#include <TFile.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TFrame.h>

using namespace std;

int main(int argc, char **argv) {

  cout << endl << "Current tools included in PlottingTool:" << std::endl;
  cout << "*******************************************" << endl << endl;

  cout << "PlotResponse" << endl;
  cout << "usage: PlotResponse myFile.root" << endl;
  cout << "options: --histname name" << endl << endl;
  cout << "In current iteration myFile.root must include a TH2 plotting response Vs. Pt.  The name of this hist can be set using" << endl;
  cout << "the --histname option, and is hResponseProbePT by default.  The response will be fit in each Pt bin, and a canvas of the fit results" << endl;
  cout << "will be saved in myFile.root, along with a TGraphError showing the response as a function of Pt." << endl;

  cout << endl << "*******************************************" << endl << endl;

  cout << "doSyst" << endl;
  cout << "usage: doSyst --data dataFile.root --MC MCFile.root --jetAlg JetAlgorithmName" << endl;
  cout << "options: --output outFileName.root" << endl << endl;
  cout << "Compares the response measured in two samples using the TGraphs created by PlotResponse.  All items contained in these imputs begining " << endl;
  cout << "with bootstrap are assumed to be TH2Bootstraps, and these are then used to obtain the systematic uncertainty on the observed response difference.  " << endl;
  cout << "The statistical uncertaties on these shifts are also determined and an alternative rebinning systematic uncertainty is also produced.  " << endl;
  cout << "Results are saved as syst_JES.root unless otherwie specified using --output." << endl; 

  return 0;
}
