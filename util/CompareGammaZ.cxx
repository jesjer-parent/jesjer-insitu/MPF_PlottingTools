#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>

#include <TFile.h>
#include <TCanvas.h>
#include <TFrame.h>
#include <TGraphErrors.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TLegend.h>


#include "PlottingTools/PlottingTools.h"

using namespace PlottingTools;

/** Example program to get JES systematics */
int main(int argc, char *argv[])
{
  SetAtlasStyle();
  int ip=1;
  int iplast = ip;
  char _namebuffer[100];
  std::string ZDataFileName = "syst_data_AntiKt4LCTopo.root"; 
  std::string ZMCFileName   = "syst_MC_AntiKt4LCTopo.root";
  std::string GDataFileName = "syst_data_AntiKt4LCTopo.root";
  std::string GMCFileName   = "syst_MC_AntiKt4LCTopo.root";
  string JetAlg        = "AntiKt4LCTopo";
  string _measurement  = "mpf";
  TString _outfile     = "syst_JES.root"; 

  while (ip<argc) {
    cout << "ip is " << ip << endl;
    if (string(argv[ip]).substr(0,2)=="--") {
      if (string(argv[ip])=="--Zdata") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          ZDataFileName=argv[ip+1];
          ip+=2;
          cout << "should be in here " << ip << endl;
        } else {std::cout<<"\nno ZData file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--ZMC" or string(argv[ip])=="--mc") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          ZMCFileName=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno ZMC file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--Gdata") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          GDataFileName=argv[ip+1];
          ip+=2;
          cout << "should be in here " << ip << endl;
        } else {std::cout<<"\nno GData file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--GMC" or string(argv[ip])=="--mc") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          GMCFileName=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno GMC file name specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--jetAlg" or string(argv[ip])=="--alg") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          JetAlg=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno jet algorithm specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--output") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _outfile=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno output specified"<<std::endl; break;}
      }
      else if (string(argv[ip])=="--measurement") {
        if (ip+1<argc && string(argv[ip+1]).substr(0,2)!="--") {
          _measurement=argv[ip+1];
          ip+=2;
        } else {std::cout<<"\nno output specified"<<std::endl; break;}
      }
    }
    if (ip == iplast) {cout << "problem with arguments " << ip << " " << iplast <<  endl; return 1;}
    iplast = ip; 
  }
 
  cout << endl << endl; 
  cout << "*****************************************" << endl << endl;
  cout << "Comparing " << ZDataFileName << " to " << ZMCFileName << endl;
  cout << "Jet Alogorithm set to: " << JetAlg << endl;
  cout << endl;
  cout << "These settings can be changed using " << endl;
  cout << "doSyst --data MyDataFileName.root --mc MyMCFileName.root --alg JetAlgorithmUsed" << endl << endl;
  cout << "*****************************************" << endl;
  cout << endl << endl;
  
  TFile *output = TFile::Open(_outfile, "recreate");
    
  TFile* Zdata = TFile::Open(TString( TString(ZDataFileName))); //"syst_data_" + JetAlg + ".root"));
  TFile* ZMC   = TFile::Open(TString( TString(ZMCFileName))); //"syst_MC_" + JetAlg + ".root"));
  TFile* Gdata = TFile::Open(TString( TString(GDataFileName))); //"syst_data_" + JetAlg + ".root"));
  TFile* GMC   = TFile::Open(TString( TString(GMCFileName))); //"syst_MC_" + JetAlg + ".root"));

  TCanvas* cComp = new TCanvas( "DataMC_Ratio", "DataMC_Ratio", 200, 10, 700, 780 );
  cComp->Divide( 1, 2, 0.0, 0.01, 0 );
  cComp->cd(1);
  gPad -> SetLogx();
  cComp->cd(2);
  gPad -> SetLogx();

  double xMax=1000;
  double xMin=10;

  if (_measurement == "mpf") PrepareTwoPadCanvas( cComp, "P_{T}^{Ref}", "R_{MPF}", "Data/MC", xMin, xMax, 0.3001, 1.09001, 0.9001, 1.09999, 505);
  else if (_measurement == "bal") PrepareTwoPadCanvas( cComp, "P_{T}^{Ref}", "p_{T}^{jet} / p_{T}^{ref}", "Data/MC", xMin, xMax, 0.8001, 1.19001, 0.9001, 1.09999, 505);

  TString nameChunk = "Response";
  if (_measurement == "bal") nameChunk = "Balance";
  TGraphErrors* ZDataGraph = (TGraphErrors*) Zdata->Get(nameChunk+"VsProbePT_graph");
  ZDataGraph->SetMarkerStyle(20);
  ZDataGraph->SetMarkerSize(1.0);
  ZDataGraph->SetMarkerColor(1);
  
  TGraphErrors* ZMCGraph   = (TGraphErrors*) ZMC->Get(nameChunk+"VsProbePT_graph");
  ZMCGraph->SetMarkerStyle(20);
  ZMCGraph->SetMarkerSize(1.0);
  ZMCGraph->SetMarkerColor(2);

  TGraphErrors* GDataGraph = (TGraphErrors*) Gdata->Get(nameChunk+"VsProbePT_graph");
  GDataGraph->SetMarkerStyle(24);
  GDataGraph->SetMarkerSize(1.0);
  GDataGraph->SetMarkerColor(1);

  TGraphErrors* GMCGraph   = (TGraphErrors*) GMC->Get(nameChunk+"VsProbePT_graph");
  GMCGraph->SetMarkerStyle(24);
  GMCGraph->SetMarkerSize(1.0);
  GMCGraph->SetMarkerColor(2);

  TGraphErrors* gZRatio = new TGraphErrors(); 
  gZRatio->SetMarkerStyle(20);
  gZRatio->SetMarkerSize(1.0);
  gZRatio->SetMarkerColor(1);

  TGraphErrors* gGRatio = new TGraphErrors();
  gGRatio->SetMarkerStyle(24);
  gGRatio->SetMarkerSize(1.0);
  gGRatio->SetMarkerColor(1);

  for ( int n = 0; n < ZDataGraph->GetN(); ++n ) {
    if (ZDataGraph->GetY()[n] == 0 or ZMCGraph->GetY()[n]==0) continue;
    if (ZDataGraph->GetY()[n] == -999 or ZMCGraph->GetY()[n]==-999) continue;
    gZRatio->SetPoint(n, 0.5*(ZDataGraph->GetX()[n] + ZMCGraph->GetX()[n]), ZDataGraph->GetY()[n] / ZMCGraph->GetY()[n]);
    cout << (0.5*(ZDataGraph->GetX()[n] + ZMCGraph->GetX()[n])) << " " << (ZDataGraph->GetY()[n] / ZMCGraph->GetY()[n]) << endl;
    double yerr = ZDataGraph->GetY()[n] / ZMCGraph->GetY()[n] * sqrt( pow(ZDataGraph->GetEY()[n]/ZDataGraph->GetY()[n],2) + pow(ZMCGraph->GetEY()[n]/ZMCGraph->GetY()[n],2) );
    gZRatio->SetPointError(n, ZDataGraph->GetEX()[n], yerr);
  }

  for ( int n = 0; n < GDataGraph->GetN(); ++n ) {
    if (GDataGraph->GetY()[n] == 0 or GMCGraph->GetY()[n]==0) continue;
    if (GDataGraph->GetY()[n] == -999 or GMCGraph->GetY()[n]==-999) continue;
    gGRatio->SetPoint(n, 0.5*(GDataGraph->GetX()[n] + GMCGraph->GetX()[n]), GDataGraph->GetY()[n] / GMCGraph->GetY()[n]);
    cout << (0.5*(GDataGraph->GetX()[n] + GMCGraph->GetX()[n])) << " " << (GDataGraph->GetY()[n] / GMCGraph->GetY()[n]) << endl;
    double yerr = GDataGraph->GetY()[n] / GMCGraph->GetY()[n] * sqrt( pow(GDataGraph->GetEY()[n]/GDataGraph->GetY()[n],2) + pow(GMCGraph->GetEY()[n]/GMCGraph->GetY()[n],2) );
    gGRatio->SetPointError(n, GDataGraph->GetEX()[n], yerr);
  }
 

  TLegend *leg = new TLegend(0.2,0.6,0.4,0.8,NULL,"brNDC");
//  TLegend *leg = new TLegend(0.2,0.2,0.4,0.4,NULL,"brNDC");
  leg->SetBorderSize(0);
  leg->AddEntry(ZDataGraph, "Z, Data", "lp");
  leg->AddEntry(ZMCGraph,   "Z, Powheg+Pythia", "lp");
  leg->AddEntry(GDataGraph, "#gamma, Data", "lp");
  leg->AddEntry(GMCGraph,   "#gamma, Powheg+Pythia", "lp");


  TF1 *LineAtCenter = PlotConstantLine( 1.0 , xMin, xMax, 2, 1 );
  TF1 *LineAtMinus  = PlotConstantLine( 0.95, xMin, xMax, 3, 1 );
  TF1 *LineAtPlus   = PlotConstantLine( 1.05, xMin, xMax, 3, 1 );

  LineAtCenter->Draw("sameL");
  LineAtMinus->Draw("sameL");
  LineAtPlus->Draw("sameL");

  cComp->cd(1);
  ZDataGraph->Draw("sameP");
  ZMCGraph->Draw("sameP");
  GDataGraph->Draw("sameP");
  GMCGraph->Draw("sameP");
  leg->Draw("same");
  cComp->cd(2);
  gZRatio->Draw("sameP");
  gGRatio->Draw("sameP");


  
   
  sprintf( _namebuffer, "%s_Ratio.pdf", _measurement.c_str());
  cComp->SaveAs(_namebuffer);
  output->cd();
  output->Close();
  return 0;
}

